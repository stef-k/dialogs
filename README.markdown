dialogs
=======

A Python module containing shotcuts to simple dialogs

# Contents:
* msg, a message box
* yesno, a yes or no – OK or cancel dialog
* entry, a user input dialog
* filechoose, a file chooser dialog
* dirchoose, a directory chooser dialog
* radio, a radio group dialog
* form, a form like dialog

# Dependencies:
* Python, GTK+3 with PyGObject (successor of PyGtk)
  tested on Ubuntu 12.04

# Usage:
Import the module and check the following:

General guidelines: optional arguments are inside <>

* Usage: msg(message, <title>, <"msg_type">)
* Usage: yesno("message", <title>, <"msg_type">)->Boolean
* Usage: entry(question, <title>)->string
* Usage: filechoose(<title>)->filepath as string
* Usage: dirchoose(<title>)->directory path as string
* Usage: form(<title>, <colnames>, <rows>, <width>, <height>, <instructions>)->dict
* Usage: radio(<"title">, <"instructions">, ["choice1", "choice2"], <width>, <height>)->the index of the choice

for screenshots and code examples check http://iccode.net/?p=314
