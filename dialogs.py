#!/usr/bin/env python
# coding:utf-8
# Copyright (C) 2012  Stefanos Kariotidis <stef[at]iccode.net> http://iccode.net
# Author:   Stefanos Kariotidis--<stef@iccode.net>
# Description: Set of common needed Gtk user interface elements for direct use.
# Contents: message box, yes or no, user input, file chooser, 
# directory chooser, form, radio buttons group
# Dependencies: PyGObject (The new version of PyGtk 2) & Gtk+3
# Created: 07/11/2012
# This file is part of dialogs.py.
#
# dialogs.py  is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dialogs.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with dialogs.py.  If not, see <http://www.gnu.org/licenses/>.
"""Set of convenience common dialogs.
Available dialogs: message box(msg), yes no (yesno), user input(entry), 
file choooser(filechoose), directory chooser(dirchoose), form (form)

General guidelines: optional arguments are inside <> i.e: <argument>

Usage: msg(message, <title>, <"msg_type">)
Usage: yesno("message", <title>, <"msg_type">)->Boolean
Usage: entry(question, <title>)->string
Usage: filechoose(<title>)->filepath as string
Usage: dirchoose(<title>)->directory path as string
Usage: form(<title>, <colnames>, <rows>, <width>, <height>, <instructions>)->dict
Usage: radio(<"title">, <"instructions">, ["choice1", "choice2"], 
			<width>, <height>)->the index of the choice
"""

import os

try:
	from gi.repository import Gtk
except Exception as e:
	print "Could not load Gtk dependencies!"
	print e

#==============================================================================#
# Some global useful variables 
_INFO = Gtk.MessageType.INFO
_QUESTION = Gtk.MessageType.QUESTION
_ERROR = Gtk.MessageType.ERROR
_OTHER = Gtk.MessageType.OTHER
_WARNING = Gtk.MessageType.WARNING
#==============================================================================#

def main():
	pass

if __name__ == '__main__':
	main()

#==============================================================================#
def entry(message="This is a question", title="Question"):
	"""A simple user input box

	entry(question, <title>)->string
	"""
	_messagedialog = Gtk.MessageDialog(None, 
					flags=Gtk.DialogFlags.DESTROY_WITH_PARENT, type=_QUESTION, 
					title=title, buttons=Gtk.ButtonsType.OK, 
					message_format=message)

	_action_area = _messagedialog.get_content_area()

	_result = None
	_entry = Gtk.Entry()
	_action_area.pack_start(_entry, True, True, 0)
	_messagedialog.show_all()
	_messagedialog.run()
	
	if Gtk.ResponseType.OK:
		_result = _entry.get_text()
		_messagedialog.destroy()
		
	return _result

#==============================================================================#
def msg(message="This is the content of the message.", title="Info", 
		msg_type="info"):
	"""A simple message box.

	Usage: msg(message, <title>, <"msg_type">)
	"""
	if msg_type == "info":
		type = _INFO
	elif msg_type == "warn":
		type == _WARNING
	elif msg_type == "error":
		type = _ERROR
	elif msg_type == "question":
		type = _QUESTION
	elif msg_type == "other":
		type = _OTHER
	
	_messagedialog = Gtk.MessageDialog(None, title=title,
					flags=Gtk.DialogFlags.DESTROY_WITH_PARENT, type=type,
                                      buttons=Gtk.ButtonsType.OK,
                                      message_format=message)

	_messagedialog.show_all()
	_messagedialog.run()
	_messagedialog.destroy()

#==============================================================================#
def yesno(message="Please confirm the pending action.", title="Please Confirm", 
		buttons="okcancel", msg_type="warn"):
	"""A simple yes or now message box.

	Usage: yesno("message", <title>, <"msg_type">)->Boolean
	"""
	if msg_type == "info":
		type = _INFO
	elif msg_type == "warn":
		type = _WARNING
	elif msg_type == "error":
		type = _ERROR
	elif msg_type == "question":
		type = _QUESTION
	elif msg_type == "other":
		type = _OTHER
	
	if buttons == "okcancel":
		buttons_choise = Gtk.ButtonsType.OK_CANCEL
	else:
		buttons_choise = Gtk.ButtonsType.YES_NO
	
	_messagedialog = Gtk.MessageDialog(None, 
						flags=Gtk.DialogFlags.DESTROY_WITH_PARENT, title=title,
                                      type=type,
                                      buttons=buttons_choise,
                                      message_format=message)
	_result = False
	_response = _messagedialog.run()
	if _response == Gtk.ResponseType.OK:
			_result = True
	elif _response == Gtk.ResponseType.YES:
		_result = True
	_messagedialog.destroy()
	
	return _result
#==============================================================================#
def filechoose(title="Select File"):
	"""A file chooser dialog

	Usage: filechoose(<title>)->filepath as string
	"""

	_result = None
	_fchooser = Gtk.FileChooserDialog(title, None, Gtk.FileChooserAction.OPEN,
									("Cancel", Gtk.ResponseType.CANCEL,
									"Select File", Gtk.ResponseType.OK))

	_fchooser.set_current_folder(os.path.expanduser("~"))
	_response = _fchooser.run()
	if _response == Gtk.ResponseType.OK:
		 _result = _fchooser.get_filename()
		 
	_fchooser.destroy()
	
	return _result

#==============================================================================#
def dirchoose(title="Select Directory"):
	"""A directory chooser dialog

	Usage: dirchoose(<title>)->directory path as string
	"""

	_result = None
	_fchooser = Gtk.FileChooserDialog(title, None,
							Gtk.FileChooserAction.SELECT_FOLDER,
							("Cancel", Gtk.ResponseType.CANCEL,
									"Select Directory", Gtk.ResponseType.OK))

	_fchooser.set_current_folder(os.path.expanduser("~"))
	_response = _fchooser.run()
	if _response == Gtk.ResponseType.OK:
		_result = _fchooser.get_filename()

	_fchooser.destroy()
	
	return _result

#==============================================================================#
def form(title="Form", colnames=["Label", "Input"], rows=["Name:","Surname:"], 
width=300, height=300, instructions="Please type into empty fields"):
	"""A two column form (left labels, right input boxes) 
	
	Usage: form(<title>, <colnames>, <rows>, <width>, <height>, <instructions>)->dict
	The form can have many rows defined by their labels.
	"""

	_win = Gtk.Window()
	_win.set_title(title)
	
	# Layout manager
	_box = Gtk.Box(False, orientation=Gtk.Orientation.VERTICAL, spacing=10)
	_win.add(_box)
	
	# since we have not a class we use nestet functions for the
	# implementation of the event handlers
	def _text_edited(widget, path, text):
		"""Assigns editable cells contents to the model'"""
	
		_liststore[path][1] = text

	#create the model
	_liststore = Gtk.ListStore(str, str)
	for _label in rows:
		_liststore.append([_label, ""])

	#append the model to the view
	_treeview = Gtk.TreeView(model=_liststore)

	#create left and right column renderers
	_left_renderer = Gtk.CellRendererText()
	_right_renderer = Gtk.CellRendererText()
	_right_renderer.set_property("editable", True)

	#arrange left column
	_left_column = Gtk.TreeViewColumn(colnames[0], _left_renderer, text=0)
	_treeview.append_column(_left_column)

	#arrange right column
	_right_column = Gtk.TreeViewColumn(colnames[1], _right_renderer, text=1)
	_treeview.append_column(_right_column)
	
	_label = Gtk.Label()
	_label.set_markup("<b>Close window to save</b>")
	
	_instructions_label = Gtk.Label()
	_instructions_label.set_markup(instructions)
	
	#connect the edited event
	_right_renderer.connect("edited", _text_edited)
	
	# add widgets to the box layout manager
	_box.pack_start(_label, False, True, 0)
	_box.pack_start(_instructions_label, False, True, 0)
	_box.pack_start(_treeview, True, True, 1)
	
	_win.set_default_size(width, height)
	_win.connect("delete-event", Gtk.main_quit)
	_win.show_all()
	Gtk.main()
	_results = {}
	for _left, _right in _liststore:
		_results[_left] = _right
	return _results
	
#==============================================================================#
def radio(title="Pick a choice", 
		instructions="Please pick a choice and close window", 
		choices=["Is it a bird?","Is it a train?","Is it Superman?"],
		width=200, height=200):
	"""A dialog with radio buttons to pick one choice
	
	Usage: radio(<"title">, <"instructions">, ["choice1", "choice2"], 
				<width>, <height>)->the index of the choice
	"""
	
	# Window
	_win = Gtk.Window()
	_win.set_title(title)
	
	# Layout manager
	_box = Gtk.Box(False, orientation=Gtk.Orientation.VERTICAL, spacing=10)
	_win.add(_box)
	
	_label = Gtk.Label()
	_label.set_markup("<b>%s</b>" % instructions)
	_box.pack_start(_label, False, True, 5)
	
	
	_radios = []

	
	for choice in choices:
		if choices.index(choice) == 0:
			_radios.append(Gtk.RadioButton.new_with_label_from_widget(
							None, choices[0]))
			_radios[0].set_active(True)
		else:
			_radios.append(Gtk.RadioButton.new_with_label_from_widget(
							_radios[0], choice))

	for radio in _radios:			
		_box.pack_start(radio, False, True, 1)
		radio.set_active(False)
	
	
	_win.connect("delete-event", Gtk.main_quit)	
	_win.set_default_size(width, height)
	_win.show_all()
	Gtk.main()
	
	_result = None
	
	for item in _radios:
		if item.get_active():
			_result = _radios.index(item)
	
	return _result
#==============================================================================#
