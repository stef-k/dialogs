#!/usr/bin/env python
from distutils.core import setup

setup(name='dialogs',
      version='1.0',
      description='Set of common needed Gtk user interface elements for direct use',
      author='Stefanos Kariotidis',
      author_email='stef@iccode.net',
      license="GNU General Public License version 3 (GPLv3)",
      py_modules=['dialogs'],
     )
